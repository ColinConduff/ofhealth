
$(function() {
    // Insert info from CHANGE_ME.js into index.html
    $("#patient-portal").prop("href", info.patientPortalURL);
    $(".address").append(info.address);
    $(".cityStateZip").append(info.cityStateZip);
    $(".phone").append(info.phone);
    $(".fax").append(info.fax);
    $(".officeHoursMTWThF").append(info.officeHours.mondayThroughFriday);
    $(".officeHoursSat").append(info.officeHours.saturday);

    // Ids for navigation components 
    var componentIds = ["#home", "#services", "#financial", "#privacy", "#about"];

    // Show the component selected from the navbar
    $(".nav > li > a").click(function() {
        var id = "#" + $(this).attr('id').slice(0, -3);
        hideAllComponentsExcept(id);
        $(this).parent("li").addClass("active");
        $(id).show();
    });

    // Hide all of the components other than the selected one
    function hideAllComponentsExcept(idToExclude) {
        componentIds.forEach(function(componentId) {
            if(componentId !== idToExclude) {
                $(componentId).hide();
                $(".nav > li.active").removeClass("active");
            }
        });
    }

    // Move side panel down for smaller devices
    if ( $(window).width() < 800) {
        $("#side-panel").appendTo("#side-panel-mobile-loc");
    }
});