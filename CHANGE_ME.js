// Change the information here to easily update the website

var info = {
    address: "4941 N Town Centre Dr",
    cityStateZip: "Ozark, MO 65721",
    phone: "417-551-4810",
    fax: "417-551-4814",
    patientPortalURL: "https://mycw98.ecwcloud.com/portal13216/jsp/login.jsp",
    officeHours: {
        mondayThroughFriday: "8 am - 5 pm",
        saturday: "8 am - 12 pm"
    }
}